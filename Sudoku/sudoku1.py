#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#sudoku
from mpi4py import MPI
import numpy as np
import time
import os
import funkcije as fje
import generator as gen
from random import randrange

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

assert comm.Get_size() == 3


if rank == 0:
 print("Dobrodošli u sudoku\nAutori: Krešimir, Tina, Ivona\n")
 tez = input("Unesite tezinu koju zelite ---> l-lako, n-normalno, t-tesko\n")
 comm.send(tez, dest=1, tag=55)

if rank==1:
 time.sleep(5)
 tez = comm.recv(source=0, tag=55)
 
 os.system('clear')
 while(True):
      if str(tez) == 'l':
         tezina=randrange(30,35)
         break
      elif str(tez) == 'n':
         tezina=randrange(25,30)
         break
      elif str(tez) == 't':
         tezina=randrange(20,25)
         break
      else:
         print("Nepoznata naredba\n")
 
 comm.send(tezina, dest=2, tag=77)

if rank==2:
 time.sleep(5)
 ploca1 = np.array([('#','#','#','#','#','#','#','#','#'),
                    ('#','#','#','#','#','#','#','#','#'),
                    ('#','#','#','#','#','#','#','#','#'),
                    ('#','#','#','#','#','#','#','#','#'),
                    ('#','#','#','#','#','#','#','#','#'),
                    ('#','#','#','#','#','#','#','#','#'),
                    ('#','#','#','#','#','#','#','#','#'),
                    ('#','#','#','#','#','#','#','#','#'),
                    ('#','#','#','#','#','#','#','#','#')])

 ploca2 = np.array([('#','#','#','#','#','#','#','#','#'),
                    ('#','#','#','#','#','#','#','#','#'),
                    ('#','#','#','#','#','#','#','#','#'),
                    ('#','#','#','#','#','#','#','#','#'),
                    ('#','#','#','#','#','#','#','#','#'),
                    ('#','#','#','#','#','#','#','#','#'),
                    ('#','#','#','#','#','#','#','#','#'),
                    ('#','#','#','#','#','#','#','#','#'),
                    ('#','#','#','#','#','#','#','#','#')])
 tezina = comm.recv(source=1, tag=77)
 ploca1=gen.generiraj(ploca1, tezina)
 for i in range(0,9):
        for j in range(0,9):
                ploca2[i][j]=ploca1[i][j]

 os.system('clear')
 ploca1 = fje.ploca(ploca1)

 kraj=True

 while(kraj):
  while(True):

     kordinate=str(input("Unesite koordinate za polje ---> Npr 0.0 za nulti redak i nulti stupac\n"))
     unos=int(input("Unesite vrijednost na tim koordinatama\n"))
     if(len(kordinate)==3 and (unos>=1 and unos<=9)): #moguci brojevi 1-9
        break
     else:
        print("Pogreška pri unosu. Probajte ponovno\n")
  ploca1=fje.unos(ploca1,ploca2,kordinate,unos)
  fje.provjera_unosa(ploca1,kordinate,unos)
  ploca1=fje.ploca(ploca1)
  a=fje.provjera(ploca1)
  print(a)
  if a=="pobjeda":
      break
 os.system("pause")
