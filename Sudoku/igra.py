import funkcije as fje
import numpy as np
print("Dobrodošli u sudoku\nAutori: Krešimir, Tina, Ivona\n")


ploca=np.zeros((9,9),dtype=str)
ls=[]
for line in open( "datoteka.txt", "r" ).readlines():
    for value in line.split( '\t' ):
        ls.append( value )
j=0
k=0
for i in range(0,len(ls)):
    if i==0:
        ploca[0][0]=str(ls[0])
    elif i%9==0:
        j=j+1
        k=0
    ploca[j][k]=str(ls[i])
    k=k+1
ploca2 = np.zeros((9,9),dtype=str )

#kopiranje u plocu 2 radi provjere predefiniranih brojeva                                                                     
for i in range(0,9):
    for j in range(0,9):
        ploca2[i][j]=ploca[i][j]
fje.ploca(ploca)
kraj=True
while(kraj):
    while(True):
        kordinate=str(input("Unesite koordinate za polje ---> Npr 0.0 za nulti redak i nulti stupac\n"))
        unos=int(input("Unesite vrijednost na tim koordinatama\n"))
        if(len(kordinate)==3 and (unos>=1 and unos<=9)): #moguci brojevi 1-9                                                          
            break
        else:
            print("Pogreška pri unosu. Probajte ponovno\n")
    ploca=fje.unos(ploca,ploca2,kordinate,unos)
    fje.provjera_unosa(ploca,kordinate,unos)
    fje.ploca(ploca)
    a=fje.provjera(ploca)
    print(a)
    if a=="pobjeda":
        break
