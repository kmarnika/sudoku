#!/usr/bin/env python3
# -*- coding: utf-8 -*- 
""" funkcije koje ce biti importane u main program """
import numpy as np
import time
import os
import random
from random import randrange
import funkcije as fje

def redak(pocetna):
	"""
	Vraćanje početne ploče sa ispremiješanim redovima unutar blokova.

	Argumenti:
	pocetna -- np.array ispunjen početnim vrijednostima elemenata


	Vraća:
	Devet redaka početne tablice se dijeli na skupove od po tri retka.
	Retci unutar blokova se prizvoljno ispremiješaju povodom čega smisao
	početne ploče ostaje nepromijenjena.Nakon izmjene redoslijeda redaka
	funkcija vraća tu ploču.

	"""

	zamjena=np.zeros((9,9),dtype=np.int32)
	for i in range(0,9):
		for j in range(0,9):
			zamjena[i][j]=pocetna[i][j]

	for i in range(1,4):
		if i==1:		
			r1=randrange(0,3)
			r2=randrange(0,3)
		if i==2:
			r1=randrange(3,6)
			r2=randrange(3,6)
		if i==3:
			r1=randrange(6,9)
			r2=randrange(6,9)
		privremena=zamjena[r2]
		pocetna[r2]=zamjena[r1]
		pocetna[r1]=privremena
	return pocetna
	
def blok(pocetna):
	"""
	Vraćanje početne ploče sa ispremiješanim blokovima redova.

	Argumenti:
	pocetna -- np.array ispunjen početnim vrijednostima elemenata

	Vraća:
	Početna ploča se dijeli u blokove od po tri retka. Ukoliko zamijenimo
	redoslijed bilo koja dva bloka smisao ploče će ostati nepromijenjene.
	Odabire se proizvoljan broj od 1-4 i ovisno o tome koji je broj odabran
	zamjenjuju se 1. i 2., 1. i 3., 2. i 3. blok, ili se vraća nepromijenjena
	početna ploča.

	"""
	zamjena=np.zeros((9,9),dtype=np.int32)
	for i in range(0,9):
		for j in range(0,9):
			zamjena[i][j]=pocetna[i][j]
	r1=randrange(1,4)
	if r1==1:
		p1=zamjena[0:3]
		pocetna[0:3]=zamjena[3:6]
		pocetna[3:6]=p1	
	elif r1==2:
		p1=zamjena[0:3]
		pocetna[0:3]=zamjena[6:9]
		pocetna[6:9]=p1
	elif r1==3:
		p1=zamjena[3:6]
		pocetna[3:6]=zamjena[6:9]
		pocetna[6:9]=p1
	else:
		return pocetna
	return pocetna
		
	

def transpon(pocetna):
	"""
	Vraća transponiranu početnu matricu

	Agrumenti:
	pocetna -- np.array ispunjen početnim vrijednostima elemenata

	Vraća:
	Na temelju početne ploče izvodi njezinu transponiranu verziju te je
	vraća.

	"""	
	pocetna=pocetna.T
	return pocetna	

	
def generiraj(ploca):
        """
	Vraća ploču s ispremiješanim redovima u kojoj vrijede pravila da se svaki 
	broj smije samo jednom ponoviti unutar stupca, retka i 3x3 kvadrata.

	Argumenti:
	ploca -- np.array ispunjen vrijednostima elemenata

	Vraća:
	Ovisno o slučajno odabranom broju (1000, 2000), nad redovima ploče se vrše
	funkcije zamjene redova i blokova, te transponiranje ploče. 
        """
        pocetna = np.array([(1, 2, 3, 4, 5, 6, 7, 8, 9),
                  (4, 5, 6, 7, 8, 9, 1, 2, 3),
                  (7, 8, 9, 1, 2, 3, 4, 5, 6),
                  (2, 3, 4, 5, 6, 7, 8, 9, 1),
                  (5, 6, 7, 8, 9, 1, 2, 3, 4),
                  (8, 9, 1, 2, 3, 4, 5, 6, 7),
                  (3, 4, 5, 6, 7, 8, 9, 1, 2),
                  (6, 7, 8, 9, 1, 2, 3, 4, 5),
                            (9, 1, 2, 3, 4, 5, 6, 7, 8)],dtype=np.int32)
        r=randrange(1000,2000)
        for i in range(1,r):
                rb = randrange(1,4)
                if rb==1:
                        pocetna=redak(pocetna)
		
		
                elif rb==2:
		
                        pocetna=blok(pocetna)
                else:
                        pocetna=transpon(pocetna)
		
        for i in range(0,9):
                for j in range(0,9):
			
                        ploca[i][j]=pocetna[i][j]
        return ploca

def tezine(tezina):
	"""
	vraća ploču sa različitim brojem sakrivenih elemenata ovisno o 
	odabranoj težini.
	
	Argumenti:
	tezina -- int, ovisi o težini igre koju igrač odabere
	
	Vraća:
	Ovisno o odabranoj težini igre više ili manje polja bude sakriveno 
	znakom # te se na temelju toga vraća dobivena ploča.
	"""
	ploca=np.zeros((9,9), dtype=np.str)
	while tezina >0 :
		
		for i in range(0,9):
			r=randrange(0,9)
			while(True):
				if(ploca[i][r]=='#'):
					r=randrange(0,9)
				else:
					break
			ploca[i][r]='#'
			tezina=tezina-1
	return ploca
		
