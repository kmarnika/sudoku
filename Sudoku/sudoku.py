#!/usr/bin/env python3
# -*- coding: utf-8 -*- 
#sudoku
from mpi4py import MPI
import numpy as np
import time
import os
import funkcije as fje
import generator as gen
from random import randrange
comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()
assert comm.Get_size() == 2
if rank == 0:
    print("Dobrodošli u sudoku\nAutori: Krešimir, Tina, Ivona\n")
    tez = input("Unesite tezinu koju zelite ---> l-lako, n-normalno, t-tesko \n")
    ploca1 = np.zeros((9,9), dtype=np.int32)
    ploca1=gen.generiraj(ploca1)#generiranje brojeva za sudoku igru
    comm.Send([ploca1, MPI.INT], dest=1, tag=13)#salje se popunjena ploca
    comm.send(tez,dest=1,tag=55)
    #time.sleep(2)
if rank==1:
    ploca = np.zeros((9,9), dtype=np.str)
    ploca1 = np.zeros((9,9), dtype=np.int32)
    #time.sleep(2)
    comm.Recv([ploca1, MPI.INT], source=0, tag=13)
    tez=comm.recv(source=0, tag=55)
    #print(tez)
        
    #odabir tezine
        
   #os.system('clear')
    while(True):
        if str(tez) == 'l':
            tezina=randrange(30,35)
            break
        elif str(tez) == 'n':		
            tezina=randrange(25,30)
            break
        elif str(tez) == 't':
            tezina=randrange(20,25)
            break
        else:
            print("Nepoznata naredba\n")
    tezina_ploca=gen.tezine(tezina)#to
    for i in range(0,9):
        for j in range(0,9):
            ploca[i][j]=str(ploca1[i][j])


        #stavljanje # na generiranu plocu
        
    for i in range(0,9):
        for j in range(0,9):
            if(tezina_ploca[i][j]=='#'):
                ploca[i][j]='#'
    #os.system('clear')
    fje.ploca(ploca)
    #dat=open('datoteka.txt','w')
    np.savetxt('datoteka.txt', ploca, delimiter='\t',fmt='%s')
    #dat.close()
    print("Uspješno ste generirali novu sudoku plocu -> za igranje na posljednje generiranoj ploci upisite python3 igra.py")