#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# funkcije koje ce biti importane u main program

import numpy as np
import random
import os
from random import randrange
tezina=0

def ploca(ploca):
 """
 Vraća početnu ploču za sudoku.
 
 Argumenti:
 ploca -- niz # u obliku np.array
 
 Vraća:
 Postavlja početnu ploču za sudoku koristeći # iz np.arraya. Nakon svaka tri
 znaka # postavlja se okomita crta (|). Nakon 3 znamenki u stupcu postavljaju
 se dvije horizontalne crte.
 """
 for i in range(1,10):
    for j in range(1,10):
         if(j%3==0):
             if(i%3==0):
                  print(ploca[i-1][j-1]+" "+" | ", end="")
             else:
                  print(ploca[i-1][j-1]+" "+ " | ", end="")
         else:
             print(ploca[i-1][j-1]+" ", end="")
    print("\n")
    if(i%3==0):
         for k in range(1,12):
              print("--", end="")
         print("\n")


def unos(ploca,ploca2,kordinate,unos):
 """
 Funkcija vrši unos broja u sudoku ploču.
 
 Argumenti:
 ploca -- niz # u obliku np.array
 ploca2 -- niz # u obliku np.array
 kordinate -- koordinate za unos broja oblika int.int
 unos -- predstavlja mjesto u ploci kojoj su pridruzene koordinate -> 
 ploca[k1][k2]
 
 Vraća:
 Funkcija vraća ploču s unesenim novim brojem na koordinatama (x.y). Ukoliko
 se pokuša unijeti nova vrijednost na poziciji na kojoj je već unesena
 predefinirana vrijednost, funkcija ispisuje odgovarajauće upozorenje, inače se
 na mjesto koordinata upisuje nova vrijednost.
 """

 k1=int(kordinate[0])
 k2=int(kordinate[2])
 if ploca2[k1][k2]!='#':
     print("NE MOZETE MIJENJATI PREDEFINIRANE VRIJEDNOSTI IGRE\n")  
 else:
     ploca[k1][k2]=unos
 return ploca


def provjera(ploca):
 """
 Ispisuje se pobjeda ukoliko na ploči više nema polja sa znakom #.
 
 Argumenti:
 ploca -- np.array s popunjenim vrijednostima umjesto znaka # 
 
 Vraća:
 Funkcija vraća ispis "Pobjeda!!" ukoliko na ploči nema više polja sa znakom #.
 Ukoliko postoje polja sa znakom # ispisuje se poruka igraču da odigra 
 slijedeći potez.
 Provjerava se ispunjenost ploče po retcima, stupcima i kvadratima.
 Postoji lista koja ide od 1-9. Provjerava se pozicija u ploči sa elementom iz
 liste, te se taj element briše iz liste. Ukoliko je duljina liste > 0, 
 ispisuje se poruka igraču o neispravnoj popunjenosti retka, stupca ili 
 kvadrata.
 """

 for i in range(0,9):
     for j in range(0,9):
          if(ploca[i][j]=='#'):
                return "Odigrajte sljedeci potez"
#popunjena cijela ploca
 list=[1,2,3,4,5,6,7,8,9]
#provjera redaka
 for i in range(0,9): #redci
     for j in range(0,9):#stupci
        for k in range(0,9):#lista
             if ploca[i][j]==list[k]:
                  list.remove(list[k])
     if len(lista)>0:
            return "Redak "+str(i)+"nije ispravno popunjen"
     list=[1,2,3,4,5,6,7,8,9]
 list=[1,2,3,4,5,6,7,8,9]
#provjera stupaca
 for i in range(0,9): #stupci
     for j in range(0,9):#redci
        for k in range(0,9):#lista
             if ploca[j][i]==list[k]:
                 list.remove(list[k])
     if len(lista)>0:
        return "Stupac "+str(i)+"nije ispravno popunjen"
     list=[1,2,3,4,5,6,7,8,9]

#provjera po kvadratima
 list=[1,2,3,4,5,6,7,8,9]
 a=0
 b=3
 br=0
 while(a<=9):#dize stupce po 3-- kvadrat dole
        while(b<=9): #kvadrat desno
                for i in range(a,b):
                     for j in range(a,b):
                          if ploca[j][i]==str(list[k]):
                               list.remove(list[k])
                     if len(lista)>0:
                             return "Kvadrat "+str(br)+"nije ispravno popunjen"
                     list=[1,2,3,4,5,6,7,8,9]
                br=br+1
                b=b+3
        a=a+3
 return "Pobjeda!"

def provjera_unosa(ploca,kordinate,unos):
 """
 Ispisuje se odgovarajuća poruka o pogrešnom unosu.
     
 Argumenti:
 ploca -- np.array s popunjenim vrijednostima umjesto znaka #
 kordinate -- koordinate za unos broja oblika int.int
 unos -- broj koji je pridružen na koordinatama u ploči
       
 Vraća:
 Funkcija vraća odgovarajuću poruku o pogrešnom unosu ukoliko broj koji se 
 pokušava unijeti u ploču već postoji u tom retku, stupcu ili
 kvadratu.
 """
       
 red=0
 stup=0
 kvad=0
 k1=int(kordinate[0])
 k2=int(kordinate[2])
 unos=str(unos)
#provjera reda
 for i in range(0,9):
     if ploca[k1][i]==unos:
          red=red+1
 if red>=2:
     print("UPOZORENJE! Posljednji redak je krivo popunjen -> posljednje uneseni broj je "+ unos )
#provjera stupca	
 for i in range(0,9):
     if ploca[i][k2]==unos:
          stup=stup+1
 if stup>=2:
          print("UPOZORENJE! Posljednji stupac je krivo popunjen -> posljednje uneseni broj je "+ unos )
#provjera po kvadratima
 if (k1>=0 and k1<3) and (k2>=0 and k2<3):#prvi kvadrat
     for i in range(0,3):
          for j in range(0,3):
                if ploca[i][j]==unos:
                     kvad=kvad+1
     if kvad>=2:	
          print("UPOZORENJE! Kvadrat 1 je krivo popunjen -> posljednje uneseni broj je "+ unos)
 elif (k1>=0 and k1<3) and (k2>=3 and k2<6):#drugi kvadrat
     for i in range(0,3):
          for j in range(3,6):
                if ploca[i][j]==unos:
                     kvad=kvad+1
     if kvad>=2:	
          print("UPOZORENJE! Kvadrat 2 je krivo popunjen -> posljednje uneseni broj je "+ unos)	
 elif (k1>=0 and k1<3) and (k2>=6 and k2<9):#treci kvadrat
     for i in range(0,3):
          for j in range(6,9):
                if ploca[i][j]==unos:
                     kvad=kvad+1
     if kvad>=2:	
          print("UPOZORENJE! Kvadrat 3 je krivo popunjen -> posljednje uneseni broj je "+ unos)
 elif (k1>=3 and k1<6) and (k2>=0 and k2<3):#cetvrti kvadrat
     for i in range(3,6):
          for j in range(0,3):
                if ploca[i][j]==unos:
                     kvad=kvad+1
     if kvad>=2:	
          print("UPOZORENJE! Kvadrat 4 je krivo popunjen -> posljednje uneseni broj je "+ unos)
 elif (k1>=3 and k1<6) and (k2>=3 and k2<6):#peti kvadrat
     for i in range(3,6):
          for j in range(3,6):
                if ploca[i][j]==unos:
                     kvad=kvad+1
     if kvad>=2:	
          print("UPOZORENJE! Kvadrat 5 je krivo popunjen -> posljednje uneseni broj je "+ unos)
 elif (k1>=3 and k1<6) and (k2>=6 and k2<9):#sesti kvadrat
     for i in range(3,6):
          for j in range(6,9):
                if ploca[i][j]==unos:
                     kvad=kvad+1
     if kvad>=2:	
          print("UPOZORENJE! Kvadrat 6 je krivo popunjen -> posljednje uneseni broj je "+ unos)
 elif (k1>=6 and k1<9) and (k2>=0 and k2<3):#sedmi kvadrat
     for i in range(6,9):
          for j in range(0,3):
                if ploca[i][j]==unos:
                     kvad=kvad+1
     if kvad>=2:	
          print("UPOZORENJE! Kvadrat 7 je krivo popunjen -> posljednje uneseni broj je "+ unos)
 elif (k1>=6 and k1<9) and (k2>=3 and k2<6):#osmi kvadrat
     for i in range(6,9):
          for j in range(3,6):
                if ploca[i][j]==unos:
                     kvad=kvad+1
     if kvad>=2:	
          print("UPOZORENJE! Kvadrat 8 je krivo popunjen -> posljednje uneseni broj je "+ unos)
 elif (k1>=6 and k1<9) and (k2>=6 and k2<9):#deveti kvadrat
     for i in range(6,9):
          for j in range(6,9):
                if ploca[i][j]==unos:
                     kvad=kvad+1
     if kvad>=2:	
          print("UPOZORENJE! Kvadrat 9 je krivo popunjen -> posljednje uneseni broj je "+ unos)
 else:
     return
